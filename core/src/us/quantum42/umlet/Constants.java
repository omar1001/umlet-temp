package us.quantum42.umlet;

import com.badlogic.gdx.graphics.Color;

/**
 * Created by omar on 5/29/17.
 * Hello
 */

class Constants {
//  TODO: CHANGE INT TO SHORT AND SO ON..
    static final float INFINITY = Float.MAX_VALUE;
    static String HIGH_SCORE = "hs";

    //player mode. aiming & shooting

    // player constants.
    static final float PLAYER_CENTER_X_TO_SCREEN_WIDTH_RATIO = 1f / 2f;
    static final float PLAYER_CENTER_Y_TO_SCREEN_HEIGHT_RATIO = 1f / 4f;//3f / 16f; //1f / 4f; // 5f / 16f;
    static final float PLAYER_RADIUS_TOUCH_CIRCLE_RATIO_TO_MIN_DIMENSION = 1f / 14f;
    static final float DOT_DISTANCE_RATIO = 1/10;



    //Balls
    static final float BALLS_SPAWN_RATE = 1f;
    static final float BALL_RADIUS_RATIO_TO_MIN_DIMENSION = 1f / 18f; //1f / 24f;
    static final float FALLING_BALLS_RANDOM_LIMIT_FACTOR = 13f;
    static final float NORMAL_BALL_INITIAL_VELOCITY_RATIO_TO_HEIGHT = 1f / 90f;// initial velocity
    static final float NORMAL_BALL_FULL_SPEED_INTERVAL_IN_SECONDS = 50000f;// the time when ball reach max speed in seconds
    static final int NORMAL_BALL_INTERNAL_CIRCLES = 2;
    static final int NORMAL_BALL_INITIAL_CIRCLES_NUM = 1;
    static final int NORMAL_BALL_TIMES_TO_INCREMENT_REMAININ_CIRCLES = 5;



    //Bullets
    static final float BULLET_RADIUS_RATIO_TO_MIN_DIMENSION = 1f / 68f; //1f / 56f; //1f / 78f;
    static final float NORMAL_BULLET_SPEED_DELTA_FACTOR = 0.5f;
    static final float INTERVAL_LIMIT_NORMAL_BULLET = 2f;//the time between spawning each bullet
    static final float NORMAL_BULLET_TIME_LIMIT = 2.5f;// time for a bullet to reach the virtual circle

    static final float BALL_SCORE_DRAW_TIME_LIMIT = 2f;


    //Colors
    static final float MAIN_SCREEN_BACKGROUND_R = 255f;
    static final float MAIN_SCREEN_BACKGROUND_G = 255f;
    static final float MAIN_SCREEN_BACKGROUND_B = 255f;

    static final Color NORMAL_BALL_COLOR = new Color(Color.valueOf("E85050"));
    static final Color NORMAL_BALL_ALTER_COLOR = new Color(Color.valueOf("BFDEF4"));
    static final Color NORMAL_BULLET_COLOR = new Color(Color.valueOf("0450FB"));
    static final Color PLAYER_GUN_BASE_COLOR = Color.BLACK;
    static final Color PLAYER_GUN_AIMING_BORDER_COLOR = Color.BLACK;
    static final Color PLAYER_AIMING_LINE_COLOR = Color.BLACK;
    static final Color PLAYER_AIMING_FINGER_REF_COLOR = new Color(Color.valueOf("D1D1D7"));


    static final short RIGHT_SIDE = 0;
    static final short LEFT_SIDE = 1;
    static final short UP_SIDE = 2;






}
