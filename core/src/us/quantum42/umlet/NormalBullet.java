package us.quantum42.umlet;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;

import static java.lang.Math.abs;

/**
 * Created by omar on 6/16/17.
 * Hello
 */

class NormalBullet extends Bullet {
    public static float radius = 0;
    private int hitMultiplyer = 1;
    private boolean ballScoreDraw = false;
    private Vector2 ballCenterForScoreDraw;
    private float ballScoreDrawTime = 0;
    private BitmapFont font;
    private SpriteBatch batch;
    private VirtualCircle collideRespVCircle;



    NormalBullet(Viewport viewport, Vector2 start, Vector2 end, FallingBalls fallingBalls) {
        super(viewport, start, end, fallingBalls);
        NormalBullet.radius = Constants.BULLET_RADIUS_RATIO_TO_MIN_DIMENSION * Math.min(viewport.getScreenHeight(), viewport.getScreenWidth());
        batch = new SpriteBatch();
        font = new BitmapFont();
        font.setColor(Color.BLACK);
        font.getData().setScale(2, 2);
        collideRespVCircle = new VirtualCircle(viewport.getScreenHeight());
    }

    @Override
    void update(float delta) {

        time += delta;

        //there is a problem here!!
        // it's not a general velocity, this velocity depends on the difference between end and start
        // thats not acceptable, as if there is two balls moving regardless the reason at the same
        // start but different end parallel to each others, both of them will reach the final destination
        // at the same time that's not realistic velocity it should be constant velocity
        position.x = ((time / Constants.NORMAL_BULLET_TIME_LIMIT) * (end.x - start.x)) + start.x;
        position.y = ((time / Constants.NORMAL_BULLET_TIME_LIMIT) * (end.y - start.y)) + start.y;

//        if (end.x == start.x) {
//            Gdx.app.log("----------X", "X == X bro, end: " + end + ", start: " + start + ", position: " + position);
//        } if (end.y == start.y) {
//            Gdx.app.log("----------Y", "Y == Y bro, start.y: " + start.x);
//        }

        // if the bullet didn't collide with the wall check it's collision with the falling balls
        if (!checkWallCollision()) {
            fallingBalls.balls.begin();
            NormalBall ball;
            for (int i=0; i < fallingBalls.balls.size; i++) {
                ball = (NormalBall) fallingBalls.balls.get(i);
                if (this.position.dst(ball.position) <= NormalBullet.radius + ((NormalBall)ball).radius) {
                    if (!fallingBalls.hitBall(i)) {
                        //TODO: here make the animation that the normal bullet do to the ball,
                        //  todo: if the ball didn't send false, this is for a case of that the ball
                        //  todo: might be a bonus for something.
                    }
                    collide(ball);
                    if (ball.isLastHit()) {
                        fallingBalls.balls.removeIndex(i);
                        UmletGame.currentScore++;
                    }
                }
            }
            fallingBalls.balls.end();
        }

        /*
        if (ballScoreDraw) {
            ballScoreDrawTime += delta;
            if (ballScoreDrawTime > Constants.BALL_SCORE_DRAW_TIME_LIMIT) {
                ballScoreDraw = false;
                ballScoreDrawTime = 0f;
            }
        }
        */
    }


    @Override
    void render(ShapeRenderer shapeRenderer) {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(0.6078f, 0.6078f, 0.6078f, 0);
        shapeRenderer.circle (
                this.position.x,
                this.position.y,
                NormalBullet.radius,
                32
        );
        shapeRenderer.end();
        /*
        if (ballScoreDraw) {
            batch.begin();
            font.draw(batch, String.valueOf(hitMultiplyer/2), ballCenterForScoreDraw.x, ballCenterForScoreDraw.y); //you can change the position as you like
            batch.end();
        }
        */
    }


    private void collide (Ball ball) {
        //reset time
        time = 0f;
        PlayScreen.normalBallHitSound.play(1.0f);
        //create new virtual circle object & get the new end of the ball.

        collideRespVCircle.update(position, ball.position);
        start = position.cpy();
        end = collideRespVCircle.aimingEndPoint.cpy();
    }

    @Override
    public void collideWall(short side) {
        switch (side) {
            case (Constants.RIGHT_SIDE) : {
                position.x = viewport.getScreenWidth() - radius;
                break;
            }
            case (Constants.LEFT_SIDE) : {
                position.x = radius;
                break;
            }
            case (Constants.UP_SIDE) : {
                position.y = viewport.getScreenHeight() - radius;
                break;
            }
        }

        end = end.cpy();
        if (side != Constants.UP_SIDE) {
            //the mirror point of the sides left and right
            end.x = start.x;
            end.y = 2 * position.y - start.y;
        } else {
            end.y = start.y;
            end.x = 2 * position.x - start.x;
        }

        start = position.cpy();
        float d1 = viewport.getScreenHeight();
        float d0 = end.dst(start);
        float t = d1 / d0;

        end = new Vector2((1 - t) * start.x + t * end.x, (1 - t) * start.y + t * end.y);

        time = 0f;


    }

    @Override
    public boolean checkWallCollision() {
        if (position.x - radius <= 0) {
            //call left wall collision event
            collideWall(Constants.LEFT_SIDE);
            return true;
        } else if (position.x + radius >= viewport.getScreenWidth()) {
            //call right wall collision event
            collideWall(Constants.RIGHT_SIDE);
            return true;
        } else if (position.y + radius >= viewport.getScreenHeight()) {
            collideWall(Constants.UP_SIDE);
            return true;
        }
        return false;
    }

    @Override
    public boolean isOut() {
        return position.y + radius <= 0;
    }
}
