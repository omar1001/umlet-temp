package us.quantum42.umlet;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;


/**
 * Created by omar on 5/29/17.
 * Hello
 *
 * two stages:
 *      1> drag the aiming.
 *      2> shot the bullet.
 */

class Player implements InputProcessor{

    private Viewport viewport;
    static Vector2 center;
    static float touchCircleRadius;
    private int pointerOnFocus = -1; // -1 means that it's not aiming other wise is the id of the pointer that's aiming
    private Vector2 aimingPoint;
    private boolean AIMING = false;
    private VirtualCircle virtualCircle; // player bullets object uses this object as it's sent by reference, so don't use this object for other than aiming.
    private PlayerBullets playerBullets;
    private float gunbaseCircleRadius;
    private float dottedLineCircleRadius;
    FallingBalls fallingBalls;
    private Vector2 playerCenter;



    private int dotDist;// = Constants.DOT_DISTANCE_RATIO;

    Player (Viewport viewport, FallingBalls fallingBalls) {
        this.viewport = viewport;
        this.fallingBalls = fallingBalls;
        init();
    }

    void init() {
        resize();
    }

    //update the center & the radius of the player because it's relative to the screen size.
    void resize() {
        center = new Vector2(0, 0);

//        center = new Vector2(
//                viewport.getScreenWidth() * Constants.PLAYER_CENTER_X_TO_SCREEN_WIDTH_RATIO,
//                viewport.getScreenHeight() * Constants.PLAYER_CENTER_Y_TO_SCREEN_HEIGHT_RATIO
//        );
        touchCircleRadius = Math.min (viewport.getScreenHeight(), viewport.getScreenWidth())
                * Constants.PLAYER_RADIUS_TOUCH_CIRCLE_RATIO_TO_MIN_DIMENSION;
        gunbaseCircleRadius = touchCircleRadius / 3;
        dottedLineCircleRadius = touchCircleRadius / 10;
        virtualCircle = new VirtualCircle(Math.max(viewport.getScreenWidth(), viewport.getScreenHeight()));
        playerBullets = new PlayerBullets(viewport, fallingBalls);
        playerBullets.init(virtualCircle);
        dotDist = 50;// = Math.round(viewport.getScreenHeight() * Constants.DOT_DISTANCE_RATIO);
        int x = Math.round(viewport.getScreenHeight() * Constants.DOT_DISTANCE_RATIO);
        center.x = viewport.getScreenWidth() / 2;
        center.y = 0;
    }

    void update (float delta) {
        //UmletGame.currentScore = Math.round(1 / delta);
        //  call update in the bullet if in shooting mode.
        //  update aiming line if in aiming mode.
        playerBullets.update(delta);

    }

    void render (ShapeRenderer shapeRenderer) {

        Gdx.gl.glLineWidth(3);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);

        //bounds of input drag
        shapeRenderer.setColor(Constants.PLAYER_GUN_AIMING_BORDER_COLOR);
        shapeRenderer.circle(
                center.x,
                center.y,
                touchCircleRadius,
                32
        );
        shapeRenderer.end();

        //gun base.
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(0.6078f, 0.6078f, 0.6078f, 0);
        shapeRenderer.circle(
                center.x,
                center.y,
                gunbaseCircleRadius,
                32
        );
        shapeRenderer.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);

        if (AIMING) {
            // draw the aiming line.

            shapeRenderer.setColor(Constants.PLAYER_AIMING_LINE_COLOR);
            shapeRenderer.line(
                    center,
                    virtualCircle.aimingEndPoint
            );
            // this part for the line that is reflected from the wall
            if (virtualCircle.aimingEndPoint.x > viewport.getScreenWidth()) {
                shapeRenderer.line (
                        center.x + (viewport.getScreenWidth() - center.x) * 2 ,
                        center.y,
                        virtualCircle.aimingEndPoint.x - (virtualCircle.aimingEndPoint.x - viewport.getScreenWidth()) * 2,
                        virtualCircle.aimingEndPoint.y
                );
            } else if (virtualCircle.aimingEndPoint.x < 0) {
                shapeRenderer.line (
                        -center.x,
                        center.y,
                        virtualCircle.aimingEndPoint.x * -1,
                        virtualCircle.aimingEndPoint.y

                );
            }
            //drawDottedLine(shapeRenderer, dotDist, center.cpy(), virtualCircle.aimingEndPoint.cpy());
            shapeRenderer.end();

            // this part for the small dot that is the reference for the aiming center
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.setColor(0, 0, 0, 0);
            shapeRenderer.circle(
                    playerCenter.x,
                    playerCenter.y,
                    gunbaseCircleRadius / 3,
                    32
            );

        }
        shapeRenderer.end();

        //TODO: render finger aiming circle.

        playerBullets.render(shapeRenderer);

    }

    private void drawDottedLine(ShapeRenderer shapeRenderer, int dotDist, Vector2 v1, Vector2 v2) {

        //only dots
        shapeRenderer.begin(ShapeRenderer.ShapeType.Point);

        Vector2 vec2 = v2.sub(v1);
        float length = vec2.len();
        for(int i = 0; i < length; i += dotDist) {
            vec2.clamp(length - i, length - i);
            shapeRenderer.circle(v1.x + vec2.x, v1.y + vec2.y, dottedLineCircleRadius);
        }

        shapeRenderer.end();
    }

    // According game story of continuous shooting while aiming:
    //  this method is will call the shooting method in player bullets object
    //  shoot method will terminate shooting by calling a method in player bullets object.
    private void setAiming (float x, float y) {
        aimingPoint = viewport.unproject(new Vector2(x, y));
        if (aimingPoint.dst(center) <= gunbaseCircleRadius) {
            return;
        }
        // check if the point is outside the touch circle then handle the aiming.
        AIMING = true;
        adjustPoints();
        virtualCircle.update(center, aimingPoint);
        playerBullets.setContinuousShootingMode(true);
    }
    private void adjustPoints() {
        aimingPoint.y = aimingPoint.y - playerCenter.y;
        aimingPoint.x = aimingPoint.x - playerCenter.x + center.x;
    }

    // This method is for shooting terminator.
    private void shoot() {
        AIMING = false;

        //check if the aiming point is outside the touch circle.
        if (aimingPoint.dst(center) <= touchCircleRadius) {
            return;
        }
        playerBullets.setContinuousShootingMode(false);
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (pointerOnFocus == -1) {
            playerBullets.setContinuousShootingMode(false);
            playerBullets.init(virtualCircle);
            Vector2 point = viewport.unproject(new Vector2(screenX, screenY)); // we don't use unproject because we already using screenviewport
            pointerOnFocus = pointer;
            playerCenter = point;
            /*
            if (point.dst(center) <= touchCircleRadius) {
                pointerOnFocus = pointer;
            } else {
                pointerOnFocus = -1;
            }
            */
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        //shot if pointer on focus is not -1, & set it to -1.
        if (pointer == pointerOnFocus) {
            setAiming(screenX, screenY);
            shoot();
            pointerOnFocus = -1;
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (pointerOnFocus == pointer) {
            setAiming(screenX, screenY);
        }
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

}
