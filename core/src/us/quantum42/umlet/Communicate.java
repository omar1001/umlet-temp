package us.quantum42.umlet;

/**
 * Created by omar on 8/18/17.
 */

public interface Communicate {
    void playerDied();
    void setHighScore(int score);
    void setCurrentScore(int currentScore);
}
