package us.quantum42.umlet;

import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.Random;

import javax.swing.text.View;



/**
 * Created by omar on 6/16/17.
 * Hello
 */

class NormalBall extends Ball {

    float radius;
    float secondaryRadius;
    int remainingCircles;
    float circlesDifferenceFactor;
    float fontX;
    float fontY;
    float x;
    float y;
    float diam;
    private GlyphLayout layout;



    int order = -1;
    int spaces = 0;
    

    NormalBall(Viewport viewport) {
        super(viewport);
        init();
    }
    NormalBall(Viewport viewport, int order, int spaces, int remainingCircles) {
        this(viewport);
        this.order = order;
        this.spaces = spaces;
        this.remainingCircles = remainingCircles;
        layout = new GlyphLayout();
        init();
    }

    @Override
    void init() {
        super.init();
        radius = Math.min(viewport.getScreenHeight(), viewport.getScreenWidth())
                * Constants.BALL_RADIUS_RATIO_TO_MIN_DIMENSION;
        float ballX;
        if (order != -1) {
            ballX = new Random().nextFloat() * (viewport.getScreenWidth()/spaces - radius * 2);
            ballX +=  (order * (viewport.getScreenWidth()/spaces));
        } else {
            ballX = new Random().nextFloat() * viewport.getScreenWidth();
        }
        if (ballX - radius < 0) {
            ballX = radius;
        } else if (ballX + radius > viewport.getScreenWidth()) {
            ballX = viewport.getScreenWidth() - radius;
        }
        position = new Vector2(
                ballX,
                viewport.getScreenHeight() + radius
        );

        circlesDifferenceFactor = radius / (Constants.NORMAL_BALL_INITIAL_CIRCLES_NUM * 2);

        secondaryRadius = radius - radius / 8;
        diam = radius * 2;

    }

    @Override
    void update(float delta) {
        super.update(delta);
        layout.setText(FallingBalls.numbersBitmapFont, remainingCircles + "");

        fontX = position.x - radius + (radius*2 - layout.width) / 2;
        fontY = position.y - radius + (radius*2 + layout.height) / 2;
        x = position.x - radius;
        y = position.y - radius;

    }

    @Override
    void render(ShapeRenderer shapeRenderer) {
        /*
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        /*
        float curCirRad = radius;
        for (int i=0; i<remainingCircles; i++) {
            if (i % 2 == 0) {
                shapeRenderer.setColor(Constants.NORMAL_BALL_COLOR);
            } else {
                shapeRenderer.setColor(Constants.NORMAL_BALL_ALTER_COLOR);
            }
            shapeRenderer.circle(
                    position.x,
                    position.y,
                    curCirRad
            );
            curCirRad -= (radius / Constants.NORMAL_BALL_INITIAL_CIRCLES_NUM);
        }
        */

        /*
        float curCirRad = radius;
        for (int i=0; i<remainingCircles * 2; i++) {
            if (i % 2 == 0) {
                shapeRenderer.setColor(Constants.NORMAL_BALL_COLOR);
            } else {
                shapeRenderer.setColor(Constants.NORMAL_BALL_ALTER_COLOR);
            }
            shapeRenderer.circle(
                    position.x,
                    position.y,
                    curCirRad,
                    32
            );
            curCirRad -= circlesDifferenceFactor;
        }
*/
        /*
        shapeRenderer.setColor(0, 0, 0, 0);
        shapeRenderer.circle(
                position.x,
                position.y,
                radius,
                32
        );
        */
        /*
        shapeRenderer.setColor(255, 255, 255, 0);
        shapeRenderer.circle(
                position.x,
                position.y,
                secondaryRadius,
                32
        );
        */

        //shapeRenderer.end();

        FallingBalls.spriteBatch.begin();
        FallingBalls.spriteBatch.draw(FallingBalls.normalBallTexture, x, y, diam, diam);
        FallingBalls.spriteBatch.end();


        FallingBalls.spriteBatch.begin();
        FallingBalls.numbersBitmapFont.draw(FallingBalls.spriteBatch, layout, fontX, fontY);
        FallingBalls.spriteBatch.end();
    }

    @Override
    boolean isTotallyOutSideScreen() {
        // Not Generic
        return (position.y + radius < 0);


        //Generic:
//        if (position.x - radius > viewport.getScreenWidth() || position.x + radius < 0) {
//            return true;
//        } else if (position.y - radius > viewport.getScreenHeight() || position.y + radius < 0) {
//            return true;
//        }
//        return false;
    }

    @Override
    public boolean madePlayerLose() {
        return (isTotallyOutSideScreen() || (Player.center.dst(this.position) <= (Player.touchCircleRadius + this.radius)));
    }

    @Override
    public boolean isLastHit() {
        remainingCircles--;
        return remainingCircles <= 0;
    }
}
